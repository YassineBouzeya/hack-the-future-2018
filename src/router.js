import Vue from 'vue';
import Router from 'vue-router';
import Keypad from './components/challenge-1/Keypad.vue';
import Decode from './components/challenge-2/Decode.vue';
import Map from './components/challenge-3/Map.vue';
import Start from './components/Start.vue';
import Chart from './components/challenge-4/Charts.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/keypad',
      name: 'keypad',
      component: Keypad,
    },
    {
      path: '/decode',
      name: 'decode',
      component: Decode,
    },
    {
      path: '/map',
      name: 'map',
      component: Map,
    },
    {
      path: '/start',
      name: 'start',
      component: Start,
    },{
      path: '/chart',
      name: 'chart',
      component: Chart,
    },
  ],
});
