import axios from 'axios';

axios.defaults.baseURL = 'http://involved-htf-js-2018-prod.azurewebsites.net/api/challenge';
axios.defaults.headers.common['x-team'] = 'choose';

// CHALLENGE 1
export async function enterPasscode(code) {
  return axios.post(
    '/1',
    { sequence: code },
  );
}
// END CHALLENGE 1

// CHALLENGE 2
export async function getStreamInsights() {
  return axios.get('/2');
}

export async function submitDecodedMessage(sentence) {
  return axios.post(
    '/2',
    { sentence },
  );
}
// END CHALLENGE 2

// CHALLENGE 3
export async function getMap() {
  return axios.get('/3');
}

export async function move(x, y) {
  return axios.post(
    '/3/move',
    { x, y },
  );
}

export async function revealPiramids(piramids) {
  let positions = [];

  piramids.map(p => {
    positions.push({
      x: p.x,
      y: p.y
    });
  });

  return axios.post(
    '/3',
    { positions: positions },
  );
}

export async function openDoor(x, y) {
  return axios.post(
    '/3/final',
    { x, y },
  );
}
// END CHALLENGE 3

// CHALLENGE 4
export async function getChartData() {
  return axios.get('/4');
}

export async function postAverage(avg) {
  return axios.post(
    '/4',
    { avg: Math.round(avg) }
  )
}
// END CHALLENGE 4

